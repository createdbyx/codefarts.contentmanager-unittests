﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.ContentManager.Tests.Models
{
    public class TestModel
    {
        public string FileName { get; set; }

        public int IntegerValue { get; set; }

        public float FloatValue { get; set; }
    }
}
